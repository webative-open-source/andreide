# **Andrei Linux** installation guide

This document is a guide for installing **Andrei Linux**, an [Arch Linux](https://www.archlinux.org) based distribution. Read this document once than follow the steps in the [official guide](https://wiki.archlinux.org/index.php/Installation_guide) and [general recommendations](https://wiki.archlinux.org/index.php/General_recommendations) for installing [Arch Linux](https://www.archlinux.org) and come back here to complete the specific configuration details for this distribution.

> `Do not reboot before completing the steps in this document!`

# Specific installation instructions

## User configuration (more details [here](https://wiki.archlinux.org/index.php/Users_and_groups#User_management))

Add a user:

```
# useradd -m -G wheel -s /bin/bash arch
```

Create a password for user **arch**:

```
# passwd arch
```

## Configure the keyboard for specific language layout, e.g. for Romanian run:

```
# localectl set-keymap ro
```

## Enable Network time synchronization

```
# timedatectl set-ntp true

```

## Install initial mandatory packages

```
# pacman -Sy base-devel git rsync wget
```

## Optional bootloader configuration

#### Proceed as per instructions from here https://wiki.archlinux.org/index.php/Systemd-boot

#### Configure the loader

```
# vi /boot/loader/entries/arch.conf

title   Andrei Linux
linux   /vmlinuz-linux
initrd  /intel-ucode.img
initrd  /initramfs-linux.img
options root=/dev/sdaX rw
```

```
# vi /boot/loader/loader.conf

#timeout  3
default  arch
console-mode max
editor no
```

## Give group "wheel" sudo rights

```
# visudo
```

Uncomment the line "% wheel ALL=(ALL) ALL"

## Install _paru_

```
# pacman -S --asdeps go
# su arch
$ cd /tmp
$ wget --output-document=PKGBUILD https://aur.archlinux.org/cgit/aur.git/plain/PKGBUILD?h=paru
$ makepkg -i
$ exit
```

## Make and install the addon package

Edit /etc/pacman.conf and uncomment following lines:

```
[multilib]
Include = /etc/pacman.d/mirrorlist
```

Edit /etc/pacman.conf and add the Andrei DE repository:

```
[webative.net]
SigLevel = Optional TrustAll
Server = https://arch.webative.net/extra/$arch
```

Install `andreide` package:

```
# rm -f /usr/lib/openbox/openbox-autostart
# gpg --recv-keys 8DAE21ABA9980DBE
# pacman -Sy andreide
```

## Review modified configuration files

```
# pacdiff -l
```

## Finish

```
# reboot
```

# Optional things to do after rebooting

## BackupPC

Do this if you need to backup this machine to an external backup server with the _BackupPC_ software:

```
# useradd -d /var/lib/backuppc -m -r -s /bin/bash backuppc

# sudo -u backuppc ssh-keygen

# sudo -u backuppc vi ~/.ssh/authorized_keys
```

Add the public key of the backup server:

```
# visudo

backuppc ALL= NOPASSWD: /bin/rsync, /bin/nice
```

> Connect once to this machine from the backup server, with the `backuppc` user. This will add the server's public key to this machine's `know_hosts` file.
