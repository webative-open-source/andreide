# Maintainer: Radu Birzu <radu@webative.net>
pkgname=andreide
pkgver=r311.da4b054
pkgrel=1
pkgdesc="An openbox based desktop environment focused on productivity and speed"
arch=('x86_64')
url="https://gitlab.com/webative-open-source/andreide"
license=('GPL3')
depends=('acpi' 'acpid' 'adobe-source-sans-pro-fonts' 'alsa-utils' 'ant-dracula-kvantum-theme-git' 'arch-install-scripts' 'avahi' 'baobab' 'bluez' 'blueman' 'bluez-libs' 'bluez-utils' 'cantarell-fonts' 'ccze' 'conky' 'copyq' 'crda' 'cronie' 'cups' 'cups-filters' 'dconf-editor' 'dkms' 'dnsmasq' 'dracula-gtk-theme' 'dracula-openbox-theme' 'dunst' 'evince' 'feh' 'ffmpegthumbnailer' 'file-roller' 'fish' 'fortune-mod' 'fuseiso' 'fzf' 'galculator' 'gedit' 'git' 'gnome-icon-theme' 'gnome-keyring' 'gnome-logs' 'gnome-screenshot' 'gparted' 'gptfdisk' 'gst-libav' 'gst-plugins-bad' 'gst-plugins-good' 'gst-plugins-ugly' 'gtk-engine-murrine' 'gtk2-perl' 'gvfs-mtp' 'gvfs-smb' 'haveged' 'htop' 'kvantum' 'lead-git' 'lib32-alsa-plugins' 'lib32-libpulse' 'linux' 'linux-headers' 'lm_sensors' 'lxappearance-obconf' 'lxinput' 'lxrandr' 'hdparm' 'man-db' 'mate-polkit' 'mlocate' 'mobile-broadband-provider-info' 'modemmanager' 'most' 'mplayer' 'networkmanager' 'network-manager-applet' 'noto-fonts' 'noto-fonts-cjk' 'noto-fonts-emoji' 'nss-mdns' 'ntfs-3g' 'obconf' 'obkey' 'oblogout-py3-git' 'obmenu' 'obmenu-generator' 'openbox' 'openssh' 'openvpn' 'osmo' 'p7zip' 'papirus-icon-theme' 'pacman-contrib' 'pavucontrol' 'perl' 'perl-gtk3' 'perl-file-desktopentry' 'picom' 'pkgfile' 'plank' 'pnmixer' 'powertop' 'poppler' 'pulseaudio' 'pulseaudio-alsa' 'pulseaudio-bluetooth' 'pulseaudio-ctl' 'python-notify2' 'python2-pyxdg' 'qt5-tools' 'qt5-graphicaleffects' 'qt5-styleplugins' 'redshift' 'rsync' 'sakura' 'samba' 'screen' 'sddm' 'seahorse' 'skippy-xd-git' 'sox' 'sshfs' 'sshuttle' 'systemd-ui' 'terminus-font' 'thunar' 'thunar-archive-plugin' 'thunar-media-tags-plugin' 'thunar-volman' 'tint2' 'tlp' 'ttf-anonymous-pro' 'ttf-dejavu' 'ttf-droid' 'ttf-bitstream-vera' 'ttf-fira-code' 'ttf-liberation' 'ttf-roboto' 'ttf-ubuntu-font-family' 'tumbler' 'ulauncher' 'unrar' 'unzip' 'usb_modeswitch' 'viewnior' 'vim' 'vim-airline' 'vim-ctrlp' 'vim-fugitive' 'vim-nerdtree' 'vim-supertab' 'wget' 'whois' 'wol' 'x11vnc' 'xautolock' 'xcape' 'xclip' 'xcursor-breeze' 'xdg-user-dirs' 'xdotool' 'xlockmore' 'xorg-fonts-misc' 'xorg-server' 'xorg-xbacklight' 'xprintidle' 'xsel' 'zenity' 'zip')
optdepends=('acetoneiso2: An all in one ISO tool'
  'aisleriot: A collection of patience games'
  'anki: Helps you remember facts (like words/phrases in a foreign language) efficiently'
  'arandr: Provide a simple visual front end for XRandR (set up multi monitor environment)'
  'audacious: Lightweight, advanced audio player focused on audio quality'
  'authenticator: Two-Factor Authentication code generator for Gnome'
  'bitwarden: Password manager - Desktop Application'
  'bleachbit: Deletes unneeded files to free disk space and maintain privacy'
  'cheese: Take photos and videos with your webcam, with fun graphical effects'
  'chromium: A web browser built for speed, simplicity, and security'
  'cloudflare-warp-bin: Cloudflare Warp Client'
  'code: Visual Studio Code (vscode): Editor for building and debugging modern web and cloud applications (the open source version)'
  'firefox: Standalone web browser from mozilla.org'
  'filezilla: Fast and reliable FTP, FTPS and SFTP client'
  'fisher: A package manager for the fish shell'
  'gufw: Uncomplicated way to manage your Linux firewall'
  'gimp: GNU Image Manipulation Program'
  'icedtea-web: Additional components for OpenJDK - Browser plug-in and Web Start implementation'
  'inkscape: Professional vector graphics editor'
  'key-mon: A screencast utility that displays your keyboard and mouse status'
  'krita: Edit and paint images'
  'libinput-gestures: Actions gestures on your touchpad using libinput'
  'libreoffice-fresh: LibreOffice branch which contains new features and program enhancements'
  'linux-lts: The LTS Linux kernel and modules'
  'linux-zen: The Linux ZEN kernel and modules'
  'mailspring: A beautiful, fast and maintained fork of Nylas Mail by one of the original authors'
  'meld: Compare files, directories and working copies'
  'paru: Feature packed AUR helper'
  'pinta: Drawing/editing program modeled after Paint.NET. A simplified alternative to GIMP for casual users'
  'playonlinux: GUI for managing Windows programs under linux'
  'obhud: Script for handling laptop-specific keys and events in Openbox'
  'obsidian-appimage: A powerful knowledge base that works on top of a local folder of plain text Markdown files'
  'redsocks2-git: Transparent redirector for socksify'
  'remmina: remote desktop client written in GTK+'
  'safeeyes: A tool to reduce and prevent repetitive strain injury (RSI)'
  'screenkey: A screencast tool to display your keys inspired by Screenflick'
  'seahorse: GNOME application for managing PGP keys'
  'simple-scan: Simple scanning utility'
  'system-monitoring-center: System performance and usage monitoring tool'
  'synergy: Share a single mouse and keyboard between multiple computers'
  'thermald: The Linux Thermal Daemon program from 01.org'
  'thunderbird: Standalone mail and news reader from mozilla.org'
  'tlpui: A GTK user interface for TLP written in Python'
  'transmission-qt: Fast, easy, and free BitTorrent client'
  'ttf-inconsolata: Monospace font for pretty code listings and for the terminal'
  'ttf-symbola: Font for emoji support.'
  'v4l2loopback-dkms: v4l2-loopback device (for using a DSLR camera as an webcam)'
  'vlc: Multi-platform MPEG, VCD/DVD, and DivX player'
  'zim: A WYSIWYG text editor for a desktop wiki')
makedepends=('rsync' 'tar')
install="${pkgname}.install"
srcdir=('.')

package() {
  rm -rf usr/share/andreide/config
  tar xf usr/share/andreide/config.tar -C usr/share/andreide

  # copy files preserving timestamps and permissions
  rsync -a ./ "$pkgdir/"
}
