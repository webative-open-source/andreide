#!/bin/bash

# License: GPL v3.0 http://www.gnu.org/licenses/gpl.html

# Scripted for Andrei Desktop Environment (https://gitlab.com/webative-open-source/andreide/)
# Radu Bîrzu (radu AT webative.net)
# https://webative.net

# Description:
# Wrapper for xlock program

# Details:
# Locks the screen and automatically unlocks it after specified timeout.
# This gives parents more control of their children screen time.

settingsFile=~/.config/xlock-kids/settings
settingsFileBackup=~/.config/xlock-kids/.backup
persistentStorageFile=~/.config/xlock-kids/.storage
actionsFile=~/.config/xlock-kids/.actions
pidFile=~/.config/xlock-kids/.`basename $0`.pid

# Default settings
declare -A settings
settings[isActive]=true
settings[isBlocked]=false
settings[breakTime]=3600
settings[playTime]=3600
settings[playBreakEndSound]=true
settings[breakEndSoundFile]=/usr/share/xlock-kids/playtime.wav
settings[processesToPause]=""

# defaults for xlock
# view available fonts with "xlsfonts" command
xlockFont="-*-misc-dejavu sans-medium-r-normal-*-*-*-*-*-*-*-*-*"

lockCommand=/bin/xlock
log="logger -t "`basename $0`" -s"

# Make sure the config directory exists, for persistent storage and configuration file
mkdir -p ~/.config/xlock-kids

# State-Action-Model variables (from sam.js.org)
# Global variables
declare -A model
declare -a actions


# Helper functions
isValidEnvironment() {
	# Do some dependency checking
	if ! which logger 1>/dev/null 2>&1; then echo "Error: \"logger\" was not found!"; return 1; fi
	if [[ ${settings[playBreakEndSound]} ]] && ! which aplay 1>/dev/null 2>&1
	then
		$log "Error: \"aplay\" was not found! It is needed for playing break end sound"
		return 1
	fi
	if ! which date 1>/dev/null 2>&1; then $log "Error: \"date\" was not found!"; return 1; fi
	if ! which kill 1>/dev/null 2>&1; then $log "Error: \"kill\" was not found!"; return 1; fi
	if ! which grep 1>/dev/null 2>&1; then $log "Error: \"grep\" was not found!"; return 1; fi
	if ! which pkill 1>/dev/null 2>&1; then $log "Error: \"pkill\" was not found!"; return 1; fi
	if ! which ps 1>/dev/null 2>&1; then $log "Error: \"ps\" was not found!"; return 1; fi
	if ! which sleep 1>/dev/null 2>&1; then $log "Error: \"sleep\" was not found!"; return 1; fi
	if ! which xlock 1>/dev/null 2>&1; then $log "Error: \"xlock\" was not found!"; return 1; fi
	if ! which xset 1>/dev/null 2>&1; then $log "Error: \"xset\" was not found!"; return 1; fi

	return 0
}

showUsage() {
	echo "Starts a play/break loop, imposing a break after a period of play time."
	echo "Usage: "`basename $0`" [-d] | [-t <minutes>] | [-f] | [-s] | [-l] | [-n] | [-y] | [-h] | [-v]"
	echo "Options:"
	echo -e "\t -d: start the program in daemon mode"
	echo -e "\t -t <minutes>: add time"
	echo -e "\t -f: force the break now"
	echo -e "\t -s: end the break now"
	echo -e "\t -l: enable permanently screen lock"
	echo -e "\t -n: disable screen locking"
	echo -e "\t -y: enable screen locking"
	echo -e "\t -v: show status"
	echo -e "\t -h: show this help message"
}

displaytime() {
	local T=$1
	local D=$((T/60/60/24))
	local H=$((T/60/60%24))
	local M=$((T/60%60))
	local S=$((T%60))
	(( $D == 1 )) && printf '%d day ' $D
	(( $D > 1 )) && printf '%d days ' $D
	(( $H == 1 )) && printf '%d hour ' $H
	(( $H > 1 )) && printf '%d hours ' $H
	(( $M == 1 )) && printf '%d minute' $M
	(( $M > 1 )) && printf '%d minutes' $M
	# (( $D > 0 || $H > 0 || $M > 0 )) && (( $S > 0 )) && printf ' and '
	# (( $S > 1 )) && printf '%d seconds' $S
	# (( $S == 1 )) && printf '%d second' $S
	(( $M == 0 && $S > 1 )) && printf '%d seconds' $S
	(( $M == 0 && $S == 1 )) && printf '%d second' $S
}

isProcessRunning() {
	[[ ! -f $pidFile ]] && return 1 # return false

	while IFS='=' read key val
	do
	 [[ $key == "pid" ]] && declare pid=$val
	 [[ $key == "name" ]] && declare name=$val
	done < $pidFile

	[[ -z $pid || -z $name ]] && rm -f $pidFile && return 1 # return false

	# check if process is running
	[[ `ps -p $pid | grep -c $name` -ge 1 ]] && return 0 # return true

	# return false
	rm -f $pidFile && return 1
}

saveModelToFile() {
	echo "# state data" > $persistentStorageFile
	for key in "${!model[@]}"; do
		[[ -n ${model[$key]} ]] && echo $key"="${model[$key]} >> $persistentStorageFile
	done
}

saveSettingsToFile() {
	echo "# `basename $0` settings" > $settingsFile
	echo >> $settingsFile
	for key in "${!settings[@]}"
	do
		case "$key" in
			isActive)	echo "# Enable/Disable switch" >> $settingsFile ;;
			isBlocked)	echo "# if true, screen will be permanently blocked" >> $settingsFile ;;
			breakTime) echo "# break time (in seconds)" >> $settingsFile ;;
			playTime)	echo "# play time (in seconds)" >> $settingsFile ;;
			playBreakEndSound)	echo "# play a sound after break ends" >> $settingsFile ;;
			breakEndSoundFile)	echo "# sound file to play at break's end" >> $settingsFile ;;
			processesToPause)
				echo "# processes to pause during the break (between quotes and separated by a space e.g. \"chromium VirtualBox\")" >> $settingsFile
				;;
		esac

		[[ -z ${settings[$key]} ]] && echo -n "#" >> $settingsFile
		echo $key"="${settings[$key]} >> $settingsFile

		echo >> $settingsFile
	done

	[[ ! -f $settingsFileBackup ]] && cp $settingsFile $settingsFileBackup
}

getSettingsFromFile() {
	local -

	# get settings from file
	if [[ -f $settingsFile ]]
	then
		# Get settings from file
		while IFS='=' read name val
		do
				if [[ $name != \#* && $val ]]
				then
					# assert valid configuration values
					case "$name" in
						playTime)
							[[ ! $val =~ ^[\-0-9]+$ ]] && val=${settings[playTime]}
							(( $val < 60 )) && val=60
							;;
						breakTime)
							[[ ! $val =~ ^[\-0-9]+$ ]] && val=${settings[breakTime]}
							(( $val < 60 )) && val=60
							;;
						isActive)
							[[ $val != true && $val != false ]] && val=${settings[isActive]}
							;;
						isBlocked)
							[[ $val != true && $val != false ]] && val=${settings[isBlocked]}
							;;
						playBreakEndSound)
							[[ $val != true && $val != false ]] && val=${settings[playBreakEndSound]}
							;;
						breakEndSoundFile)
							[[ ! -f "$val" ]] && val=${settings[breakEndSoundFile]}
							;;
					esac

					# populate global settings array
					settings[$name]=$val
				elif [[ $val ]]
				then
					if [[ -n ${settings[$name]} ]]
					then
						key=${name:1}
						settings[$key]=
					fi
				fi
		done < $settingsFile
	fi

	# persist eventually changed settings values to storage
	saveSettingsToFile
}

getModelFromFile() {
	local -
	declare -A tmp

	# Get settings from persistent storage
	getSettingsFromFile

	# set model default values
	model[counter]=${settings[breakTime]}
	model[locked]=true

	# if no persistent file exists generate one here
	if [[ ! -f $persistentStorageFile ]]
	then
		model[counter]=${settings[playTime]}
		model[locked]=false
		saveModelToFile
	fi

	# Get model from persistent storage
	while IFS='=' read name val
	do
			[[ $name != \#* && $val ]] && tmp[$name]=$val
	done < $persistentStorageFile

	# update model with correct values from file
	[[ ${tmp[counter]} =~ ^[\-0-9]+$ && ${tmp[counter]} -ge 0 ]] && model[counter]=${tmp[counter]}
	[[ ${tmp[locked]} =~ ^(true|false)$ ]] && model[locked]=${tmp[locked]}
	[[ ${tmp[userVolume]} =~ ^[\-0-9]+$ && ${tmp[userVolume]} -ge 0 && ${tmp[userVolume]} -le 100 ]] && model[userVolume]=${tmp[userVolume]}
	[[ ${tmp[lockCommandPid]} =~ ^[\-0-9]+$ && `ps -p ${tmp[lockCommandPid]} | grep -c xlock` -ge 1 ]] && model[lockCommandPid]=${tmp[lockCommandPid]}

	# compute the actual state, taking into consideration the elapsed offline time
	lastModifiedTime=`stat --format=%Y $persistentStorageFile`
	timeNow=`date +%s`
	offlineElapsedTime=$(( $timeNow - $lastModifiedTime ))

	if (( offlineElapsedTime > 1 ))
	then
		if [[ ${model[locked]} == true ]]
		then
			model[counter]=$(( model[counter] - offlineElapsedTime ))
			if (( ${model[counter]} <= 0 ))
			then
				model[counter]=${settings[playTime]}
				model[locked]=false
			fi
		else
			(( $offlineElapsedTime >= ${settings[breakTime]} )) && model[counter]=${settings[playTime]}
		fi
	fi

	# persist the model in case it was modified above
	saveModelToFile
}

getActionsFromFile() {
	# Get next actions from file
	if [[ -f $actionsFile ]]
	then
		while read action
		do
			actions+=($action)
		done < $actionsFile

		rm -f $actionsFile
	fi
}

# State-Action-Model code (from sam.js.org)

model-init() {
	getModelFromFile
	# lock the screen
	${model[locked]} && ${settings[isActive]} && model[toggleLock]=true
}

model-present() {
	local -
	counter=$1
	unset model[toggleLock]

	case "$counter" in
		-10) # lock action code
			if ! ${model[locked]}
			then
				model[locked]=true
				model[counter]=${settings[breakTime]}
				model[toggleLock]=true
				settings[isActive]=true && saveSettingsToFile
			fi
			;;
		-20) # unlock action code
			if ${model[locked]}
			then
				model[locked]=false
				model[counter]=${settings[playTime]}
				model[toggleLock]=true
				settings[isBlocked]=false && saveSettingsToFile
			fi
			;;
		-30) # block action code
			if ! ${settings[isBlocked]}
			then
				settings[isBlocked]=true && settings[isActive]=true && saveSettingsToFile
				if ! ${model[locked]}
				then
					model[locked]=true
					model[counter]=${settings[breakTime]}
					model[toggleLock]=true
				fi
			fi
			;;
		-50) # enable action code
			if ! ${settings[isActive]}
			then
				settings[isActive]=true && saveSettingsToFile
				${settings[isBlocked]} &&	model[locked]=true && model[toggleLock]=true
			fi
			;;
		-60) # disable action code
			if ${settings[isActive]}
			then
				settings[isActive]=false && saveSettingsToFile
				model[counter]=${settings[playTime]}
				if ${model[locked]}
				then
					model[locked]=false
					model[toggleLock]=true
				fi
			fi
			;;
		-70) # time action code
			${settings[isActive]} && model[counter]=$(( ${model[counter]} + $time ))
			;;
		*)
			# state is counting / enabled / blocked
			if (( ${model[counter]} <= 0 ))
			then
				# counter reached 0
				model[toggleLock]=true
				if ${model[locked]}
				then
					# unlock screen
					model[locked]=false
					model[counter]=${settings[playTime]}
				else
					# lock screen
					model[locked]=true
					model[counter]=${settings[breakTime]}
				fi
			else
				if ${settings[isActive]} && ! [[ ${settings[isBlocked]} == true && ${model[locked]} == true ]]
				then
					if ${model[locked]}
					then
						model[counter]=$counter
					else
						if xset -q | grep -q "Monitor is On"
						then
							# display is on
							if [[ -n ${model[monitorOff]} ]]
							then
								timeNow=`date +%s`
								offlineElapsedTime=$(( $timeNow - ${model[monitorOff]} ))
								# check if display was off for more than break time and reset the playing time
								(( $offlineElapsedTime >= ${settings[breakTime]} )) && counter=${settings[playTime]}
								model[monitorOff]=
							fi

							model[counter]=$counter
						else
							# display is off
							[[ -z ${model[monitorOff]} ]] && model[monitorOff]=`date +%s`
						fi
					fi
				else
					# reset the display off time, if this was the case, since we don't care anymore about it
					model[monitorOff]=
				fi
			fi
		;;
	esac

	saveModelToFile
	state-render
}

# View functions
view-locked() {
	local -

	# mute the sound
	userVolume=`volume | awk -F% '{print $1}' 2>/dev/null`
	volume 0  1>/dev/null 2>&1

	# Message for xlock
	if ${settings[isBlocked]}
	then
		INFO="The screen is locked undefinetly. Enter your password to end it now :)"
	else
		timeNow=`date +%s`
		INFO="The break ends at "`date --date='@'$(( $timeNow + ${model[counter]} )) +%R`". Enter your password to end it now :)"
	fi

	# xlock option "-geometry 1x1-1-1" makes screen transparent during break
	$lockCommand -info "$INFO" -mode blank +grabserver +showdate -echokeys -echokey '*' +usefirst +description +timeelapsed -showdate -mousemotion -timeout 10 -bg "#1B1125" -fg "#CFD8DC" -font "$xlockFont" -planfont "$xlockFont" -username " " -password "" &
	lockCommandPid=$!

	if [[ -n ${settings[processesToPause]} ]]
	then
		processList=(${settings[processesToPause]})
		for processName in "$processList"
		do
			pkill -s STOP $processName 1>/dev/null 2>&1
		done
	fi


	if ${settings[isBlocked]}
	then
		$log "Screen is locked undefinetly. Waiting for unlock command or user to enter the password..."
	else
		$log "Screen locked. Waiting for "`displaytime ${model[counter]}`" to unlock..."
	fi

	# Save helper data to model here
	model[userVolume]=$userVolume
	model[lockCommandPid]=$lockCommandPid
	model[processList]=${settings[processesToPause]}
	saveModelToFile
}

view-unlocked() {
	local -

	# stop xlock process
	if [[ -n ${model[lockCommandPid]} && `ps -p ${model[lockCommandPid]} | grep -c xlock` -ge 1 ]]
	then
		kill -9 ${model[lockCommandPid]} 1>/dev/null 2>&1

		if ${settings[playBreakEndSound]}
		then
			if which aplay
			then
				volume 100  1>/dev/null 2>&1 # max out the sound
				aplay ${settings[breakEndSoundFile]} 1>/dev/null 2>&1
			fi
		fi
	fi

	# resume processes paused before the break
	if [[ -n ${model[processList]} ]]
	then
		processList=(${model[processList]})
		for processName in "$processList"
		do
			pkill -CONT $processName 1>/dev/null 2>&1
		done
	fi

	# restore the sound volume to the previous value
	[[ -n ${model[userVolume]} ]] && volume ${model[userVolume]} 1>/dev/null 2>&1

	if ${settings[isActive]}
	then
		if ${settings[isBlocked]}
		then
			$log "Screen unlocked. Playing for "`displaytime ${model[counter]}`" and then permanently lock ..."
		else
			$log "Screen unlocked. Playing for "`displaytime ${model[counter]}`" until next break..."
		fi
	else
		$log "Screen unlocked. "`basename $0`" is disabled"
	fi

	unset model[lockCommandPid] model[processList] model[userVolume]
	saveModelToFile
}

# State functions
state-representation() {
	[[ -z ${model[toggleLock]} ]] && return

	if ${model[locked]}
	then
		view-locked
	else
		view-unlocked
	fi
}

state-nextAction() {
	local -

	# check for scheduled user actions
	getActionsFromFile
	if (( ${#actions[@]} > 0 ))
	then
		oldestAction=${actions[0]} && unset actions[0]
		case "$oldestAction" in
			lock)
				actions-lock
				;;
			unlock)
				actions-unlock
				;;
			block)
				actions-block
				;;
			enable)
				actions-enable
				;;
			disable)
				actions-disable
				;;
			*) # time action
				IFS=':' read -ra timeData <<< "$oldestAction"
				time=${timeData[1]}
				[[ $time ]] && actions-time $time
				;;
		esac

		return
	fi

	# reset data if user unlocked the screen by entering the password
	if [[ -n ${model[lockCommandPid]} && `ps -p ${model[lockCommandPid]} | grep -c xlock` -eq 0 ]]
	then
		actions-unlock
		return
	fi
}

state-render() {
	state-representation
	state-nextAction
}

# Actions
actions-decrement() {
	local counter=$1
	sleep 1s
	counter=$(( counter - 1 ))
	model-present $counter
}

actions-lock() {
	 model-present -10 # -10 is the code for lock action
}

actions-unlock() {
	model-present -20 # -20 is the code for unlock action
}

actions-block() {
	model-present -30 # -30 is the code for block action
}

actions-enable() {
	model-present -50 # -50 is the code for enable action
}

actions-disable() {
	model-present -60 # -60 is the code for disable action
}

actions-time() {
	local time=$1
	model-present -70 $time # -70 is the code for time action
}


# Parse program arguments - actions
while getopts "dfhlnst:uvy" OPTION; do
	case "$OPTION" in
		d)
			isProcessRunning && echo `basename $0`" is already running!" && exit 1

			! isValidEnvironment && exit 1

			# save the current process ID into the run file
			echo -e "pid="$$"\nname="`basename $0` > $pidFile

			# Restore settings file if it is corrupted
			if [[ -f $settingsFile ]]
			then
				restoreFromBackup=false
				for key in "${!settings[@]}"
				do
					grep -q $key= $settingsFile || restoreFromBackup=true
					[[ $restoreFromBackup == true ]] && rm -f $settingsFile && break
				done

				if [[ $restoreFromBackup == true ]]
				then
					if [[ -f $settingsFileBackup ]]
					then
						for key in "${!settings[@]}"
						do
							grep -q $key= $settingsFileBackup || restoreFromBackup=false
							[[ $restoreFromBackup == false ]] && break
						done

						[[ $restoreFromBackup == true ]] && mv $settingsFileBackup $settingsFile
					fi
				fi
			fi

			# delete backup file now to force a new backup in saveSettingsToFile
			rm -f $settingsFileBackup

			# init state
			model-init

			# render initial state
			state-render

			# Start the counter decremet action loop
			while true
			do
				actions-decrement ${model[counter]}
			done
			;;
		l)
			echo "block" >> $actionsFile

			! isProcessRunning && echo `basename $0`" is not running!"
			echo "Command to enable permanent screen lock was queued."
			exit 0
			;;
		y)
			echo "enable" >> $actionsFile

			! isProcessRunning && echo `basename $0`" is not running!"
			echo "Command to enable screen locking mechanism was queued."
			exit 0
			;;
		n)
			echo "disable" >> $actionsFile

			! isProcessRunning && echo `basename $0`" is not running!"
			echo "Command to disable screen locking mechanism was queued."
			exit 0
			;;
		t)
			# manual add play time
			! isProcessRunning && echo `basename $0`" is not running!" && exit 1
			getSettingsFromFile && [[ ${settings[isActive]} == false ]] && echo `basename $0`" is disabled!" && exit 1
			[[ -z $OPTARG || ! $OPTARG =~ ^[0-9-]+$ ]] && echo "Invalid argument: "$OPTARG && showUsage && exit 1
			time=$(( $OPTARG * 60 ))
			[[ $time -eq 0 ]] && echo "Argument was zero. No time added!" && exit 1

			if (( $time < 0 ))
			then
				getModelFromFile
				if (( ${model[counter]} - ${time:1} < 60 ))
				then
					(( ${model[counter]} <= 60 )) && echo "Less than a minute left until break. No time added!" && exit 1
					time=-$(( ${model[counter]} - 60 ))
				fi
				echo -n "Command to subtract "`displaytime $(( ${time:1} ))`" from playing time was queued"
			else
				echo "Command to add "`displaytime $(( $time ))`" of playing time was queued"
			fi

			echo "time:"$time >> $actionsFile
			exit 0
			;;
		f)
			! isProcessRunning && echo `basename $0`" is not running!" && exit 1

			# check if it's necessary to lock
			getModelFromFile
			${model[locked]} && echo "screen is already locked!" && exit 1

			echo lock >> $actionsFile
			echo "Command to force the break now was queued!"
			exit 0
			;;
		s)
			! isProcessRunning && echo `basename $0`" is not running!" && exit 1

			# check if it's necessary to unlock
			getModelFromFile
			! ${model[locked]} && echo "screen is already unlocked!" && exit 1

			echo unlock >> $actionsFile
			echo "Command to end the break now was queued!"
			exit 0
			;;
		v)
			! isProcessRunning && echo `basename $0`" is not running" && exit 1

			# get data
			getModelFromFile

			# check if locking is disabled
			if ! ${settings[isActive]}
			then
				fileName=`basename $0`
				echo "$fileName is disabled"
				exit 0
			fi

			# check if blocked permanently
			if ${settings[isBlocked]} && ${model[locked]}
			then
				fileName=`basename $0`
				echo "screen is blocked permanently"
				exit 0
			fi

			if [[ ! -f $persistentStorageFile ]]; then
				echo "no pause is scheduled yet..."
				exit 0
			fi

			if [[ ${model[locked]} == true ]]; then
				timeNow=`date +%s`
				echo "break ends at "`date --date='@'$(( $timeNow + ${model[counter]} )) +%R`
				exit 0
			else
				msg=`displaytime $(( ${model[counter]} ))`" until next break"
				(( ${model[counter]} < 5 * 60 )) && msg="{{ BREAK }} is due in "`displaytime $(( ${model[counter]} ))`
				echo $msg
				exit 0
			fi

			exit 0
			;;
		h)
			showUsage
			exit 0
			;;
		?)
			showUsage
			exit 1
			;;
	esac

	exit 0
done

showUsage
exit 0
