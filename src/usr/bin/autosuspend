#!/bin/bash

# License: GPL v3.0 http://www.gnu.org/licenses/gpl.html

# Scripted for Andrei Desktop Environment (https://gitlab.com/webative-open-source/andreide/)
# Radu Bîrzu (radu AT webative.net)
# https://webative.net

# Description:
# Suspends the system when no user input is done more than the threshold time


# Example for cron, checking every minute:
#
# */1 * * * * /usr/bin/autosuspend
#

conf=/etc/andreide/autosuspend.conf

if [[ ! -f $conf ]]; then
    echo "# Autosuspend the system after a threshold idle time (true or false)" > $conf
    echo "autosuspend=false" >> $conf
    echo >> $conf
    echo "# Idle time in minutes after the system will suspend" >> $conf
    echo "threshold=30" >> $conf
fi

# Get settings
. $conf

if [[ $autosuspend == "false" ]]; then
  # Cancel autosuspend
  logger "[autosuspend]: aborted! (autosuspend is disabled)"
  exit 0
fi

if [[ $threshold =~ ^[\-0-9]+$ ]] && (( threshold > 0)); then
  # valid threshold found in config file
  threshold=$(( threshold * 60 ))
else
  logger "[autosuspend]: wrong threshold format (\"$threshold\") in config file (\"$conf\"). Using default threshold..."
  # set default threshold to 30 min
  threshold=$(( 30 * 60 ))
fi

# set initial idle time to threshold
ms=$threshold
log_message="[autosuspend]:"

# get the idle time in milliseconds
for console_users_idle_time in `who -s | awk '{ print $2 }' | (cd /dev && xargs stat -c '%n %U %X') | awk '{ print '"$(date +%s)"'-$3}'`
do
  (( $console_users_idle_time < $ms )) && ms=$console_users_idle_time # active users are connected
done

xidle=`display-env xprintidle`
if [[ $xidle =~ ^[\-0-9]+$ ]]; then
  xidle=$(( $xidle / 1000 ))
  (( $xidle < $ms )) && ms=$(( $xidle ))
fi

greeter_pid=`pgrep sddm-greeter`
if [[ $greeter_pid != "" ]]; then
  greeter_idle=`ps -p $greeter_pid -o etimes | awk 'NR>1 {print $1}'`
  (( $greeter_idle < $ms )) && ms=$(( $greeter_idle ))
fi

if [[ $ms -lt $threshold ]]; then
  logger $log_message" aborted! (it's not the time yet; threshold="$threshold"s; idle-time="$ms"s)"
  exit 0
fi

# check various processes and abort if they are running
if [[ `pgrep BackupPC_dump` ]]; then
  logger $log_message" aborted! (rsync is running)"
  exit 0
fi

if [[ `ps -ef | grep "Plex Transcoder" -c` -gt 1 ]]; then
  logger $log_message" aborted! (plex is running)"
  # reset idle time counter, by simulating "Ctrl" key press
  display-env xdotool key Ctrl
  exit 0
fi

if [[ `who | grep -c pts` -gt 0 ]]; then
  logger $log_message" aborted! (remote users connected)"
  exit 0
fi

if [[ `openbox-presentation-mode --status | grep -c enabled` -gt 0 ]]; then
  logger $log_message" aborted! (presentation mode enabled)"
  exit 0
fi

logger $log_message" suspending the system..."
 
systemctl suspend &

exit 0
