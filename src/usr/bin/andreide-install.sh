#!/bin/bash

# License: GPL v3.0 http://www.gnu.org/licenses/gpl.html

# Scripted for Andrei Desktop Environment (https://gitlab.com/webative-open-source/andreide/)
# Radu Bîrzu (radu AT webative.net)
# https://webative.net

# Description:
# Installs custom configuration files to specified users' home directories

# Defaults
override=false
answeryes=false

function show_usage {
	echo "Copies custom configuration files to \"/etc\" and \"/root\". If user names are provided, it will copy the files only to their respective home directories. Otherwise, it will copy the files to all users' directories."
	echo
	echo "Usage: "$(basename $0)" [options] [user1] [user2] [user3]..."
	echo
	echo "Options:"
	echo -e "\t-r Replaces existing files, making a backup (by default it doesn't override existing files)"
	echo -e "\t-y Answer yes to all questions (e.g. when replacing existing files) - useful in scripts"
	echo
	echo "NB: On first run, you should use the \"-r\" option and review the changes. Also when you want to restore all configuration files to their default state."
	echo

	exit 0
}

function ask {
	while true; do
		if [ "${2:-}" = "Y" ]; then
			prompt="Y/n"
			default=Y
		elif [ "${2:-}" = "N" ]; then
			prompt="y/N"
			default=N
		else
			prompt="y/n"
			default=
		fi

		# Ask the question
		read -p "$1 [$prompt] " REPLY

		# Default?
		if [ -z "$REPLY" ]; then
			REPLY=$default
		fi

		# Check if the reply is valid
		case "$REPLY" in
		Y* | y*) return 0 ;;
		N* | n*) return 1 ;;
		esac
	done
}

# Parse the command line options
while getopts "hry" OPTION; do
	case "$OPTION" in
	r)
		override=true
		shift
		;;
	y)
		answeryes=true
		shift
		;;
	?)
		show_usage
		;;
	esac
done

# Check if we have root privileges
[[ $UID != "0" ]] && echo "You must run this script as root." && exit 1

# Check if all the tools needed are installed
if ! which awk 1>/dev/null 2>&1; then echo "Error: \"awk\" was not found" && exit 1; fi
if ! which rsync 1>/dev/null 2>&1; then echo "Error: \"rsync\" was not found" && exit 1; fi
if ! which stat 1>/dev/null 2>&1; then echo "Error: \"stat\" was not found" && exit 1; fi
if ! which updatedb 1>/dev/null 2>&1; then echo "Error: \"updatedb\" was not found" && exit 1; fi

if $override && ! $answeryes; then
	echo -n "All configuration files will be restored to default (with backups). "
	! ask "Do you want to continue?" N && exit 0
	echo
fi

UPDATED_FILES=false
UPDATED_GSETTINGS=false
if $override; then
	UPDATED_FILES=true
	# Override everything (with backup)
	echo -n "Updating \"/etc\"... "
	rsync -qab --suffix=.pacsave /usr/share/andreide/config/etc/ /etc
	echo "done."
	echo -n "Updating \"/root\"... "
	rsync -qab --suffix=.pacsave /usr/share/andreide/config/root/ /root
	echo "done."
else
	# Copy only newer config files
	UPDATE_ETC=false
	src_dir=/usr/share/andreide/config/etc
	for file in $(rsync -ac --update --progress --dry-run $src_dir/ /etc); do [[ -f $src_dir/$file ]] && UPDATE_ETC=true && break; done
	if [[ $UPDATE_ETC == true ]]; then
		UPDATED_FILES=true
		echo -n "Updating \"/etc\"... "
		rsync -ac --update -b --suffix=.pacsave $src_dir/ /etc
		echo "done."
	fi

	UPDATE_ROOT=false
	src_dir=/usr/share/andreide/config/root
	for file in $(rsync -ac --update --progress --dry-run $src_dir/ /root); do [[ -f $src_dir/$file ]] && UPDATE_ROOT=true && break; done
	if [[ $UPDATE_ROOT == true ]]; then
		UPDATED_FILES=true
		echo -n "Updating \"/root\"... "
		rsync -ac --update -b --suffix=.pacsave $src_dir/ /root
		echo "done."
	fi
fi

# update dconf settings for root
if $UPDATED_FILES; then
	dconf_dir=root/.config/dconf/settings-backup
	mkdir -p $dconf_dir

	for file in $(ls $dconf_dir | grep -v .pacsave); do
		path="/${file//_/\/}/"
		current_gsettings=$(dbus-run-session -- dconf dump $path)
		# if no gsettings data was found for current path, just load it from file
		if [[ -z $current_gsettings ]]; then
			dbus-run-session -- dconf load $path <$dconf_dir/$file 2>/dev/null
			continue
		fi

		if $override || [[ -f $file.pacsave ]]; then
			dbus-run-session -- dconf dump $path >$dconf_dir/$file.pacsave 2>/dev/null
			UPDATED_GSETTINGS=true
		fi
	done
fi

users=$*
if [[ "$users" == "" ]]; then
	## get mini UID limit ##
	l=$(grep "^UID_MIN" /etc/login.defs)

	## get max UID limit ##
	l1=$(grep "^UID_MAX" /etc/login.defs)

	## use awk to print if UID >= $MIN and UID <= $MAX   ##
	users=$(awk -F':' -v "min=${l##UID_MIN}" -v "max=${l1##UID_MAX}" '{ if ( $3 >= min && $3 <= max ) print $1}' /etc/passwd)

	# get the current user
	# users=$SUDO_USER
	# [[ -z $users ]] && users=`stat -c %U ~`
fi

# Copy users' home directories
# Check if users and their home directories are valid
valid_users=
for current_user in $users; do
	current_user_home=$(awk -F: -v v="$current_user" '{if ($1==v) print $6}' /etc/passwd)
	[[ -z $current_user_home ]] && continue
	[[ ! -d $current_user_home ]] && continue
	valid_users="$valid_users $current_user"
done
users=$valid_users

# directory containing the config files for users' home
src_dir=/usr/share/andreide/config/etc/skel

# Update users' config files
for current_user in $users; do
	current_user_home=$(awk -F: -v v="$current_user" '{if ($1==v) print $6}' /etc/passwd)
	current_user_group=$(stat --format=%G $current_user_home)

	UPDATED_USER_FILES=false
	if $override; then
		# Override everything (with backup)
		UPDATED_FILES=true
		UPDATED_USER_FILES=true
		echo -n "Updating \"$current_user_home\"... "
		rsync -qab --suffix=.pacsave --chown=$current_user:$current_user_group $src_dir/* $src_dir/.[!.]* $src_dir/..?* $current_user_home 2>/dev/null
	else
		for file in $(rsync -ac --update --progress --dry-run $src_dir/ $current_user_home); do [[ -f $src_dir/$file ]] && UPDATE_USER=true && break; done
		if [[ $UPDATE_USER == true ]]; then
			UPDATED_FILES=true
			UPDATED_USER_FILES=true
			echo -n "Updating \"$current_user_home\"... "
			rsync -ac --update -b --suffix=.pacsave --chown=$current_user:$current_user_group $src_dir/* $src_dir/.[!.]* $src_dir/..?* $current_user_home 2>/dev/null
		fi
	fi

	# update gsettings
	if $UPDATED_USER_FILES; then
		dconf_dir=$current_user_home/.config/dconf/settings-backup
		mkdir -p $dconf_dir
		for file in $(ls $dconf_dir | grep -v .pacsave); do
			path="/${file//_/\/}/"
			current_gsettings=$(su $current_user -c "sh -c 'dbus-run-session -- dconf dump $path'")
			# if no gsettings data was found for current path, just load it from file
			if [[ -z $current_gsettings ]]; then
				su $current_user -c "sh -c 'dbus-run-session -- dconf load $path < $dconf_dir/$file 2>/dev/null'"
				continue
			fi

			if $override || [[ -f $file.pacsave ]]; then
				su $current_user -c "sh -c 'dbus-run-session -- dconf dump $path > $dconf_dir/$file.pacsave 2>/dev/null'"
				chown $current_user:$current_user_group $dconf_dir/$file.pacsave
				UPDATED_GSETTINGS=true
			fi
		done
	fi

	[[ $UPDATED_USER_FILES == true ]] && echo "done."
done

if $UPDATED_FILES; then
	echo -n "Updating locate database... "
	updatedb
	echo "done."
	echo
	echo "Note: You should run \"pacdiff -l\" and review all installed configuration files."
	echo
fi

if $UPDATED_GSETTINGS; then
	echo "Note: New GSettings configuration files were installed. You should ALSO run \"andreide-update-gsettings.sh\" after reviewing the GSettings configuration files with \"pacdiff\"."
	echo
fi

exit 0
