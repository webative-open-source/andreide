#!/bin/bash

# License: GPL v3.0 http://www.gnu.org/licenses/gpl.html

# Scripted for Andrei Desktop Environment (https://gitlab.com/webative-open-source/andreide/)
# Radu Bîrzu (radu AT webative.net)
# https://webative.net

# Description:
# Updates gsettings for every user with settings from custom configuration files

# FIXME: instead of loading configuartion files, use the `gsettings` tool:
# gsettings set org.gnome.desktop.interface gtk-theme "Dracula"

function show_usage {
	echo "Updates GSettings with settings found in custom configuration files. If user names are provided, it will update GSettings only for the specified users. Otherwise, it will update GSettings for every users in the system."
	echo
	echo "Usage: "$(basename $0)" [user1] [user2] [user3]..."
	echo

	exit 0
}

# Parse the command line options
while getopts "h" OPTION; do
	case "$OPTION" in
	?)
		show_usage
		;;
	esac
done

# Check if we have root privileges
[[ $UID != "0" ]] && echo "You must run this script as root." && exit 1

users=$*
if [[ "$users" == "" ]]; then
	## get mini UID limit ##
	l=$(grep "^UID_MIN" /etc/login.defs)

	## get max UID limit ##
	l1=$(grep "^UID_MAX" /etc/login.defs)

	## use awk to print if UID >= $MIN and UID <= $MAX   ##
	users=$(awk -F':' -v "min=${l##UID_MIN}" -v "max=${l1##UID_MAX}" '{ if ( $3 >= min && $3 <= max ) print $1}' /etc/passwd)
fi

# Check if users and their home directories are valid
valid_users=
for current_user in $users; do
	current_user_home=$(awk -F: -v v="$current_user" '{if ($1==v) print $6}' /etc/passwd)
	[[ -z $current_user_home ]] && continue
	[[ ! -d $current_user_home ]] && continue
	valid_users="$valid_users $current_user"
done
users=$valid_users

UPDATED_GSETTINGS=false
updated_users=
for current_user in $users; do
	current_user_home=$(awk -F: -v v="$current_user" '{if ($1==v) print $6}' /etc/passwd)

	dconf_dir=$current_user_home/.config/dconf/settings-backup
	mkdir -p $dconf_dir

	for file in $(ls $dconf_dir | grep -v .pacsave); do
		path="/${file//_/\/}/"
		su $current_user -c "sh -c 'dbus-run-session -- dconf load $path < $dconf_dir/$file'"
		UPDATED_GSETTINGS=true
	done

	if $UPDATED_GSETTINGS; then
		if [[ $updated_users=="" ]]; then
			updated_users=$current_user
		else
			updated_users="$updated_users, $current_user"
		fi
	fi
done

# update dconf settings for root
dconf_dir=root/.config/dconf/settings-backup
mkdir -p $dconf_dir

for file in $(ls $dconf_dir | grep -v .pacsave); do
	path="/${file//_/\/}/"
	dbus-run-session -- dconf load $path <$dconf_dir/$file
done

if $UPDATED_GSETTINGS; then
	echo "GSettings data was sucessfully updated for\"$updated_users\" user(s)."
else
	echo "Note: No NEW GSettings files were found. Nothing was changed!"
fi
