#!/bin/bash

# License: GPL v3.0 http://www.gnu.org/licenses/gpl.html

# Scripted for Andrei Desktop Environment (https://gitlab.com/webative-open-source/andreide/)
# Radu Bîrzu (radu AT webative.net)
# https://webative.net

# Description:
# Synchronizes existing target directories to source

verbose="--quiet"
dryrun=""
script_path=$(readlink -f ${0%/*})

user=`id -un`
home=`awk -F: -v v="$user" '{if ($1==v) print $6}' /etc/passwd`
[[ -z $home ]] && exit 1
[[ ! -d $home ]] && exit 1
output=

umask 0022

_usage ()
{
    echo "usage ${0} [options]"
    echo
    echo " General options:"
    echo "    -t <path>          Target directory"
    echo "    -s <path>          Source directory"
    echo "    -d                 Enable dry run"
    echo "    -o                 Redirect output to file"
    echo "    -v                 Enable verbose output"
    echo "    -h                 This help message"
    exit ${1}
}

while getopts 't:s:odvh' arg; do
    case "${arg}" in
        t) target="${OPTARG}" ;;
        s) source="${OPTARG}" ;;
        o) output="rsync.out" ;;
        d) dryrun="--dry-run"
          verbose="--progress -v"
          ;;
        v) verbose="--progress -v" ;;
        h) _usage 0 ;;
        *)
           echo "Invalid argument '${arg}'"
           _usage 1
           ;;
    esac
done

[[ -z ${source} || -z ${target} ]] && echo "You must provide source and target directories!" && _usage 1
# check target
goahead=true
count=0
for d in `find ${target}/* -maxdepth 0 -type d`; do
  count=$(( count + 1 ))
  data_dir=${d##*/}
  [[ ! -d ${source}/${data_dir} ]] && goahead=false && break
done

# synchronize
if [[ $goahead == true || -n $count ]]; then
  # ask root
  sudo --list > /dev/null
  echo -n "Synchronizing storage... "
  for d in `find ${target}/* -maxdepth 0 -type d`; do
    data_dir=${d##*/}
    [[ ! -d ${source}/$data_dir ]] && continue
    rsync_command="sudo rsync -a --update --delete --exclude=*~ --exclude=.*~ --exclude=**/lost+found*/ --exclude=**/.debris --exclude=**/.stfolder --exclude=workspace/ ${dryrun} ${verbose} ${source}/${data_dir}/ $target/${data_dir}"
    if [[ -n $output ]]; then
      $rsync_command 1>${output} 2>/dev/null
    else
      $rsync_command 2>/dev/null
    fi
  done
  echo "done"
else
  echo "Target directory \"${target}\" is not a valid sync directory!"
fi

