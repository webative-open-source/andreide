#!/bin/bash
# License: GPL v3.0 http://www.gnu.org/licenses/gpl.html

# Scripted for Andrei Desktop Environment (https://gitlab.com/webative-open-source/andreide/)
# Radu Bîrzu (radu AT webative.net)
# https://webative.net

# Description:
# Starts DSLR connected camera as a Webcam.
# More info at https://www.crackedthecode.co/how-to-use-your-dslr-as-a-webcam-in-linux/
# and https://www.scs.stanford.edu/~dm/blog/hide-webcam.html
#
# To load the video loopback kernel module at startup:
#
# Create /etc/modprobe.d/v4l2loopback.conf with the following content (remove the hashtag at the beginning of the line):
# options v4l2loopback nr_devices=2 exclusive_caps=1,1,1,1,1,1,1,1 video_nr=0,1 card_label="OBS Virtualcam,DSLR Webcam"
#
# Create /etc/modules-load.d/v4l2loopback.conf with the following content (remove the hashtag at the beginning of the line):
# v4l2loopback
#
# If the module was not loaded at boot, run:
# modprobe v4l2loopback
#
# The following command is starting the DSLR camera as a webcam on "/dev/video1".
# "/dev/video0" is currently hardcoded in OBS for its virtual camera.
# Remove the "hflip" filter if you don't want horizontal flip

if ! which gphoto2 1>/dev/null 2>&1; then echo "Error: \"gphoto2\" was not found" && exit 1; fi
if ! which ffmpeg 1>/dev/null 2>&1; then echo "Error: \"ffmpeg\" was not found" && exit 1; fi

gphoto2 --stdout --capture-movie | ffmpeg -i - -codec:v rawvideo -pix_fmt yuv420p -f v4l2 /dev/video1
