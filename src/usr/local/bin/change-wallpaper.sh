#!/bin/sh

# License: GPL v3.0 http://www.gnu.org/licenses/gpl.html

# Scripted for Andrei Desktop Environment (https://gitlab.com/webative-open-source/andreide/)
# Radu Bîrzu (radu AT webative.net)
# https://webative.net

# Description:
# Change desktop wallpaper

feh --no-fehbg --bg-fill -z /usr/share/sddm/themes/andreide/assets/backgrounds

exit 0
