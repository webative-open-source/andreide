#!/bin/bash

# License: GPL v3.0 http://www.gnu.org/licenses/gpl.html

# Scripted for Andrei Desktop Environment (https://gitlab.com/webative-open-source/andreide/)
# Radu Bîrzu (radu AT webative.net)
# https://webative.net

# Description:
# Displays the ACPI temperature
#
# Details: Outputs PANGO rich text suitable for Tint2 executor
#

# Dracula color palette
#
# Palette	HEX
# Background	#282a36
# Current Line	#44475a
# Foreground	#f8f8f2
# Comment	#6272a4
# Cyan		#8be9fd
# Green		#50fa7b
# Orange	#ffb86c
# Pink		#ff79c6
# Purple	#bd93f9
# Red		#ff5555
# Yellow	#f1fa8c

color=#8be9fd

temp=$(sensors -u | grep -A2 "ACPI interface" | grep "temp" | awk 'NR==2 {print $2}' | cut -d. -f1)

[[ $temp -gt 59 ]] && color=#f1fa8c
[[ $temp -gt 69 ]] && color=#ffb86c
[[ $temp -gt 89 ]] && color=#ff5555

[[ $temp -gt 0 ]] && echo "<span foreground=\"$color\">$temp °C</span>"
