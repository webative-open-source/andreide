#!/bin/bash

# License: GPL v3.0 http://www.gnu.org/licenses/gpl.html

# Scripted for Andrei Desktop Environment (https://gitlab.com/webative-open-source/andreide/)
# Radu Bîrzu (radu AT webative.net)
# https://webative.net

# Description:
# Shuffles the background wallpaper for sddm and openbox

CWD=$(dirname $0)
background_image=$(ls $CWD/assets/backgrounds | shuf -n 1)
background_file=$(awk -F "=" '/background/ {print $2}' $CWD/theme.conf)

cp $CWD/assets/backgrounds/$background_image $CWD/$background_file

exit 0
