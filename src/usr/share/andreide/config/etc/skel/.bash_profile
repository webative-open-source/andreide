#
# ~/.bash_profile
#

if [ -n "$DESKTOP_SESSION" ];then
    eval $(gnome-keyring-daemon --start)
    export SSH_AUTH_SOCK
    export QT_STYLE_OVERRIDE=gtk2
    export QT_QPA_PLATFORMTHEME=gtk2
fi

[[ -f ~/.bashrc ]] && . ~/.bashrc
