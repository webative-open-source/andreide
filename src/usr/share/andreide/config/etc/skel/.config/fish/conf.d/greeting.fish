# Dracula color palette
#
# Palette	HEX
# Background	#282a36
# Current Line	#44475a
# Foreground	#f8f8f2
# Comment	#6272a4
# Cyan		#8be9fd
# Green		#50fa7b
# Orange	#ffb86c
# Pink		#ff79c6
# Purple	#bd93f9
# Red		#ff5555
# Yellow	#f1fa8c

function fish_greeting
  # Issue
  set_color bd93f9
  echo " _____       _         _    __    _             "
  echo "|  _  |___ _| |___ ___|_|  |  |  |_|___ _ _ _ _ "
  echo "|     |   | . |  _| -_| |  |  |__| |   | | |_'_|"
  echo "|__|__|_|_|___|_| |___|_|  |_____|_|_|_|___|_,_|"
  echo

    # Keyboard Shortcuts
  set_color 6272a4
  echo -e "Ctrl-o\t\tFind a file."
  echo -e "Ctrl-r\t\tSearch through command history."
  echo -e "Alt-c\t\tcd into sub-directories (recursively searched)."
  echo -e "Alt-Shift-c\tcd into sub-directories, including hidden ones."
  echo -e "Alt-o\t\tOpen a file/dir using default editor ($EDITOR)"
  echo -e "Alt-Shift-o\tOpen a file/dir using xdg-open or open command"
  echo -e "z\t\tjump around"
  echo

    # fortune is a simple program that displays a pseudorandom message
  # from a database of quotations at logon and/or logout.
  # Type: "pacman -S fortune-mod" to install it, then uncomment the
  # following line:
  set_color ff79c6
  /usr/bin/fortune -s;
  echo

  # Get the local weather information
  set_color f1fa8c
  set report (sh -c "curl --connect-timeout 4.2 wttr.in/?format='%l|%C|%c|%t|%f|%w|%h|%p|%P|%S|%s|%m' 2>/dev/null")
  set location (echo $report | awk -F'|' '{print $1}')
  set condition_text (echo $report | awk -F'|' '{print $2}')
  set condition_icon (echo $report | awk -F'|' '{print $3}')
  set temperature (echo $report | awk -F'|' '{print $4}')
  set feels (echo $report | awk -F'|' '{print $5}')
  set wind (echo $report | awk -F'|' '{print $6}')
  set humidity (echo $report | awk -F'|' '{print $7}')
  set precipitation (echo $report | awk -F'|' '{print $8}')
  set pressure (echo $report | awk -F'|' '{print $9}')
  set sunrise (echo $report | awk -F'|' '{print $10}')
  set sunset (echo $report | awk -F'|' '{print $11}')
  set moon (echo $report | awk -F'|' '{print $12}')

  echo (set_color 8be9fd)$location (set_color f8f8f2)(date)
  echo (set_color f8f8f2)$condition_text $condition_icon(set_color f1fa8c)$temperature (set_color f8f8f2)\(feels like (set_color f1fa8c)$feels(set_color f8f8f2)\)
  echo (set_color f8f8f2)Wind (set_color ff79c6)$wind (set_color f8f8f2)Humidity (set_color bd93f9)$humidity
  echo (set_color f8f8f2)Pressure (set_color 6272a4)$pressure (set_color f8f8f2)Water (set_color 6272a4)$precipitation
  echo (set_color f8f8f2)Daylight (set_color ffb86c)$sunrise—$sunset
  echo (set_color f8f8f2)Moon phase $moon

  # Set the foreground color
  echo
  set_color f8f8f2

 end
