# Put system-wide fish configuration entries here
# or in .fish files in conf.d/
# Files in conf.d can be overridden by the user
# by files with the same name in $XDG_CONFIG_HOME/fish/conf.d

# This file is run by all fish instances.
# To include configuration only for login shells, use
# if status is-login
#    ...
# end
# To include configuration only for interactive shells, use
# if status is-interactive
#   ...
# end

if status is-interactive
	if not functions -q fisher
			set -q XDG_CONFIG_HOME; or set XDG_CONFIG_HOME ~/.config
			curl https://git.io/fisher --create-dirs -sLo $XDG_CONFIG_HOME/fish/functions/fisher.fish
			fish -c fisher 1>/dev/null 2>&1
	end

	if not fish -c 'echo $fisher_addons_installed | grep -q {$USER}'
		fish -c fisher 1>/dev/null 2>&1

		# pager
		set -U PAGER most

		# editor
		set -U EDITOR vim

		# ssh
		set -U SSH_KEY_PATH ~/.ssh/id_rsa.pub

		# fzf new keybindings
		set -U FZF_LEGACY_KEYBINDINGS 0
		# fzf TAB completion
		set -U FZF_COMPLETE 1
		# fzf enable preview window
		set -U FZF_ENABLE_OPEN_PREVIEW 1

		# notify done command
		set -U __done_notification_command 'display-env notify-send $title $message'

		# skip addons install next time
		set -Ua fisher_addons_installed {$USER}
	end
end
