function journalctl --description "Alias for colored journalctl with ccze"
    /bin/journalctl --no-hostname --reverse --quiet --catalog --no-pager $argv | ccze --raw-ansi --convert-date --options=nolookups | most
end

function l --description "Alias for ls"
    /bin/ls $argv
end

function grep
    /bin/grep --color=auto $argv
end

function ds
    /bin/du -hxd1 $argv 2>/dev/null | sort -h
end

function df
    /bin/df -h
end
