##!/usr/bin/perl
##
## SCHEMA supports the following keys: item, cat, begin_cat, end_cat,
##                                     exit, raw, sep, obgenmenu
##

=for comment

item: add an item into the menu
{item => ["command", "label", "icon"]}

cat: add a category into the menu
{cat => ["name", "label", "icon"]}

begin_cat: begin of a category
{begin_cat => ["name", "icon"]}

end_cat: end of a category
{end_cat => undef}

sep: menu line separator
{sep => undef} or {sep => "label"}

exit: default "Exit" action
{exit => ["label", "icon"]}

raw: any valid Openbox XML string
{raw => q(xml string)},

obgenmenu: category provided by obmenu-generator
{obgenmenu => "label"}

scripts: executable scripts from a directory
{scripts => ["/my/dir", BOOL, "icon"]}
BOOL - can be either true or false (1 or 0)
0 == open the script in background
1 == open the script in a new terminal

wine_apps: windows applications installed via wine
{wine_apps => ["label", "icon"]}

=cut

# NOTE:
#    * Keys and values are case sensitive. Keep all keys lowercase.
#    * ICON can be a either a direct path to a icon or a valid icon name
#    * By default, category names are case insensitive. (e.g.: X-XFCE == x_xfce)

require "$ENV{HOME}/.config/obmenu-generator/config.pl";

## Text editor
my $editor = $CONFIG->{editor};
my $terminal = $CONFIG->{terminal};
my $filemanager = $CONFIG->{filemanager};

our $SCHEMA = [
#             COMMAND                             LABEL             ICON
   {item => [$filemanager,      'File Manager',    'system-file-manager']},
   {item => [$terminal,       'Terminal',        'utilities-terminal']},
   {item => ['gnome-screenshot --interactive',  'Screenshot',       'gnome-screenshot']},
   {sep => undef},

    #          NAME            LABEL                ICON
    {cat => ['utility',     'Accessories', 'applications-accessories']},
    {cat => ['development', 'Development', 'applications-development']},
    {cat => ['education',   'Education',   'applications-science']},
    {cat => ['game',        'Games',       'applications-games']},
    {cat => ['graphics',    'Graphics',    'applications-graphics']},
    {cat => ['audiovideo',  'Multimedia',  'applications-multimedia']},
    {cat => ['network',     'Network',     'applications-internet']},
    {cat => ['office',      'Office',      'applications-office']},
    {cat => ['other',       'Other',       'applications-other']},
    {cat => ['system',      'System',      'applications-system']},


## Settings menu

    {sep => undef},
    {cat => ['settings',    'Settings',    'gnome-settings']},


## Custom "OB menu"

   {begin_cat => ['Openbox',  'gnome-settings']},
    {item => [$editor . ' ~/.conkyrc','Conky RC',$editor]},
    {item => [$editor . ' ~/.config/tint2/tint2rc','Tint2 Panel',$editor]},
    {item => [$editor . ' ~/.config/openbox/autostart','Openbox Autostart',$editor]},
    {item => [$editor . ' ~/.config/openbox/rc.xml','Openbox RC',$editor]},
    {item => [$editor . ' /etc/oblogout.conf','Openbox Logout',$editor]},
    {item => ['openbox --reconfigure','Reconfigure Openbox','terminal']},
   {end_cat   => undef},


## Use Oblogout script instead of simple exit command

   {sep => undef},
   {item => ['xlock',       'Lock Screen',      'lock']},
   {item => ['oblogout',    'Logout...',        'exit']},

    # {cat => ['qt',          'QT Applications',    'qtlogo']},
    # {cat => ['gtk',         'GTK Applications',   'gnome-applications']},
    # {cat => ['x_xfce',      'XFCE Applications',  'applications-other']},
    # {cat => ['gnome',       'GNOME Applications', 'gnome-applications']},
    # {cat => ['consoleonly', 'CLI Applications',   'applications-utilities']},
]
