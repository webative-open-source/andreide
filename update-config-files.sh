#!/bin/bash

# License: GPL v3.0 http://www.gnu.org/licenses/gpl.html

# Scripted for Andrei Desktop Environment (https://gitlab.com/webative-open-source/andreide/)
# Radu Bîrzu (radu AT webative.net)
# https://webative.net

# Description:
# Copies the host system configuration files from "/etc" to working tree and saves old versions as *.pacsave

# TODO: Also synchronize the permissions for all files and directories
#
# E.g.

function show_usage {
	echo "Copies modified configuration files from \"/etc\" to working tree and saves old versions as *.pacsave"
  echo
	echo "Note: You should run \"pacdiff -l\" and review all copied configuration files."
	echo

	exit 0
}

function ask {
  while true; do
    if [ "${2:-}" = "Y" ]; then
      prompt="Y/n"
      default=Y
    elif [ "${2:-}" = "N" ]; then
      prompt="y/N"
      default=N
    else
      prompt="y/n"
      default=
    fi

    # Ask the question
    read -p "$1 [$prompt] " REPLY

    # Default?
    if [ -z "$REPLY" ]; then
      REPLY=$default
    fi

    # Check if the reply is valid
    case "$REPLY" in
      Y*|y*) return 0 ;;
      N*|n*) return 1 ;;
    esac
  done
}

# Check if we have root privileges
[[ $UID != "0" ]] && echo "You must run this script as root." && exit 1

# Check if all the tools needed are installed
if ! which rsync 1>/dev/null 2>&1; then echo "Error: \"rsync\" was not found" && exit 1; fi
if ! which pacdiff 1>/dev/null 2>&1; then echo "Error: \"pacdiff\" was not found" && exit 1; fi
if ! which updatedb 1>/dev/null 2>&1; then echo "Error: \"updatedb\" was not found" && exit 1; fi

YELLOW='\033[1;33m'
RED='\033[1;31m'
NC='\033[0m' # No Color

dest=files/usr/share/andreide/config/etc
[[ ! -d $dest ]] && echo "\"files\" directory not found!" && exit 1

# Build the file list
FILES=/tmp/update-config-files.tmp
rm -f $FILES
for file in `rsync -ac --dry-run --progress --exclude=skel /etc/ $dest`; do
    [[ -f $dest/$file ]] && echo $file >> $FILES
done

# Copy modified/updated files
if [[ -f $FILES ]]; then
  COUNT=$(( `wc -l < $FILES` ))
  echo -n $COUNT" file(s) need to be updated. "
  ! ask "Do you want to continue?" N && rm -f $FILES && exit 0
  echo
  echo -n "Copying from \"/etc\"... "
  rsync -qab --suffix=.pacsave --no-owner --exclude=skel --files-from=$FILES /etc/ $dest
  rm -f $FILES
  echo "done."
  echo
  echo $COUNT" files were updated!"
  echo
  echo -n "Updating locate database... "
  updatedb
  echo "done."
  echo
  echo -e "Review updated configuration files and ${YELLOW}KEEP the pacsave files${NC}..."
  echo -e "!! NB: Chose to ${RED}(O)verwrite with pacsave${NC} in order to preserve the old timestamps !!"
  echo
  echo -e "NB: Only chose to remove the pacsave file and keep the newly created one, if you need to force an update!"
  echo
  pacdiff -l
  echo
else
	echo "Note: No modified/updated configuration files were found. Nothing should be done."
fi

exit 0
