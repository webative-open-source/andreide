# #SupportStallman https://rms-support-letter.github.io/

### **Andrei Linux** is a fast and practical Linux distribution based on [Arch Linux](https://www.archlinux.org).

## Rationale

*Arch Linux* is a *bleeding-edge* distribution that follows the [KISS](https://wiki.archlinux.org/index.php/Arch_terminology#KISS) principle. This paradigm fits the requirements for a modern operating system, but _Arch Linux_ is hard to install and configure as everything is done manually by the user.

*Andrei Linux* tries to create an opinionated *Arch Linux* based operating system with day-to-day software included but without the hassle of configuring everything from scratch. It follows the same *KISS* principle, with the emphasis on the speed of the desktop environment. Users are using apps and not the desktop environment.

## Installation

*Andrei Linux* is *Arch Linux* with added configuration files and shell scripts. Currently, the installation is done manually by following the instructions in the [INSTALL.md](https://gitlab.com/webative-open-source/andreide/-/blob/master/INSTALL.md) file.

## Contributing

Pull requests are welcome by everyone.

### Thanks!

![Screenshot](static/screenshot.png)
