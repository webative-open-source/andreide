#!/bin/bash

# License: GPL v3.0 http://www.gnu.org/licenses/gpl.html

# Scripted for Andrei Desktop Environment (https://gitlab.com/webative-open-source/andreide/)
# Radu Bîrzu (radu AT webative.net)
# https://webative.net

# Description:
# Prepares the arch linux extra repository

packager="Radu Bîrzu <radu@webative.net>"
dist=repo/extra/x86_64
repo_path=$dist/webative.net.db.tar.gz

# Check dependencies
if ! which git 1>/dev/null 2>&1; then echo "Error: \"git\" was not found" && exit 1; fi
if ! makepkg --check 1>/dev/null 2>&1; then makepkg --check && exit 1; fi

# Get the current version
old_version=$(grep "pkgver=.*" ./PKGBUILD | awk -F\= '{print $2}')
current_version=$(printf "r%s.%s\n" "$(git rev-list --count HEAD)" "$(git rev-parse --short HEAD)")

if [[ `ls repo/extra/x86_64/ | grep -c ${current_version}` != 0 ]]; then
  echo "A package has already been built." && exit 0
fi

YELLOW='\033[1;33m'
RED='\033[1;31m'
NC='\033[0m' # No Color

echo
echo -ne "${YELLOW}Updating version in PKGBUILD... ${NC}"
sed -i -e "s/pkgver=.*$/pkgver=$current_version/ " PKGBUILD
echo -e "${YELLOW}done.${NC}"

echo
echo -ne "${YELLOW}Making binary package... ${NC}\n"
rm -f *.tar.zst *.zst.sig
PACKAGER=$packager makepkg --force --sign --key 8DAE21ABA9980DBE
rm -f $dist/*.tar.zst $dist/*.zst.sig
mv *.tar.zst *.zst.sig $dist

echo
echo -ne "${YELLOW}Adding new package to repository... ${NC}\n"
repo-add $repo_path $dist/*.tar.zst
rm $dist/*.old
echo
echo -e "${YELLOW}Done.${NC}"

exit 0
