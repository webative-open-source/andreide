#!/bin/sh

# License: GPL v3.0 http://www.gnu.org/licenses/gpl.html

# Scripted for Andrei Desktop Environment (https://gitlab.com/webative-open-source/andreide/)
# Radu Bîrzu (radu AT webative.net)
# https://webative.net

# Description:
# Prepares the repository and commits and pushes all the changes

cd dist/extra/x86_64
# netlify is not serving symbolic links
rm -f webative.net.db && cp webative.net.db.tar.gz webative.net.db
rm -f webative.net.files && cp webative.net.files.tar.gz webative.net.files
# Generate directory listing
packages=$(find *.tar.zst -exec echo "<li><a href='/extra/x86_64/{}'>{}</a></li>\n" \;)
sed -i "s|__PACKAGES__|${packages}|" ./index.html
exit 0
